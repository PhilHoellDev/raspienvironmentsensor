from flask import Flask, render_template, request, jsonify, flash, redirect, url_for
from werkzeug.utils import secure_filename
from flask_socketio import SocketIO, emit
import os
import time
import pandas as pd
import json
from datetime import date, datetime
from apscheduler.schedulers.background import BackgroundScheduler
import atexit
#import serial
#from light import readLight
#import Adafruit_DHT
#from particles import measure_particles
#import mh_z19
#from temperatur import read_temperature_and_humidity
from database import SessionLocal, engine
import models
from sqlalchemy.sql import select
from sqlalchemy import create_engine, MetaData, Table, desc

### Global Variables ###

# Temperature and Humidity
#temperature_gpio = 4
#sensor = Adafruit_DHT.DHT22

# Database
models.Base.metadata.create_all(bind=engine)
df = pd.read_csv('data.csv')

# Sound
noice_counter = 0

# Web server and sockets
app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*")

# Definition of web sockets
@socketio.on('connect')
def handle_connect():
    print('Server: Client connected')
    
    db = SessionLocal()

    time_data, temp_data, hum_data, co2_data, light_data, pm25_data, pm10_data = [], [], [], [], [],[], []

    # Read data from database
    last_ten_records = db.query(models.Record).order_by(desc(models.Record.date)).limit(10).all()

    for element in reversed(last_ten_records):
        time_data.append(element.date)
        temp_data.append(element.temperature)
        hum_data.append(element.humidity)
        co2_data.append(element.carbondioxide)
        light_data.append(element.light)
        pm25_data.append(element.pm25)
        pm10_data.append(element.pm10)

    socketio.emit('inital_data', {'light': light_data, 'temperatur':temp_data, 'humidity':hum_data, 'co2':co2_data, 'pm25':pm25_data, 'pm10':pm10_data, 'time':time_data})
    db.close()
        

@socketio.on('noisy')
def noice_detected():
    print('Noice detected!')
    noice_counter =+1

# Sensor Method
def read_all_sensors():
    global df

    print("Reading all sensors")

    #light=readLight()
    #temperature,humidity = read_temperature_and_humidity()
    #print(temperature,humidity)
    #pm25, pm10 = measure_particles()
    #co2 = mh_z19.read_from_pwm(gpio=12,range=5000)

    light = 220
    temperature,humidity = 22, 50
    pm25, pm10 = 5, 5
    co2 = 400
    time = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    
    data = [[time,temperature,humidity,light,pm25,pm10,co2]]
    temp_df = pd.DataFrame(data, columns = ['date', 'temperature', 'humidity', 'light', 'pm25', 'pm10', 'co2'])

    # Write data to database
    db_record = models.Record(
        date = time,
        temperature = temperature,
        humidity = humidity,
        light = light,
        carbondioxide = co2,
        pm25 = pm25,
        pm10 = pm10
    )
    db = SessionLocal()
    db.add(db_record)
    db.commit()
    db.close()

    # Write data to csv
    #df = df.append(temp_df, ignore_index=True)
    #df.to_csv('data.csv', index=False)

    # Read last 10 rows from csv
    #df_10 = df.tail(10)
    #df_json = df_10.to_json()

    #socketio.emit('send_data', df_json)
    socketio.emit('send_data', {'light': light, 'temperatur':temperature, 'humidity':humidity, 'co2':co2, 'pm25':pm25, 'pm10':pm10, 'time':time})

# Scheduling sensors
scheduler = BackgroundScheduler()
scheduler.add_job(func=read_all_sensors, trigger="interval", seconds=60)
scheduler.start()

# Shut down the scheduler when exiting the app
atexit.register(lambda: cron.shutdown(wait=False))

@app.route('/')
def hello():
    
    return render_template('index.html')

@app.route('/mobile')
def mobile():
    
    return render_template('mobile.html')


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0')