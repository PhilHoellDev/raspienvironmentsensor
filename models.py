from sqlalchemy import Column, Float, Integer, String
from sqlalchemy.types import Date 
from database import Base

class Record(Base):
    __tablename__ = "Records"

    id = Column(Integer, primary_key=True, index=True)
    date = Column(String)
    temperature = Column(Float)
    humidity = Column(Float)
    light = Column(Float)
    carbondioxide = Column(Float)
    pm25 = Column(Float)
    pm10 = Column(Float)

    @property
    def serialize(self):
       """Return object data in easily serializable format"""
       return {
           'id'             : self.id,
           'time'           : self.date,
           'temperature'    : self.temperature,
           'humidity'       : self.humidity,
           'light'          : self.light,
           'co2'            : self.carbondioxide,
           'pm25'           : self.pm25,
           'pm10'           : self.pm10
       }
    
    @property
    def serialize_many2many(self):
       """
       Return object's relations in easily serializable format.
       NB! Calls many2many's serialize property.
       """
       return [ item.serialize for item in self.many2many]