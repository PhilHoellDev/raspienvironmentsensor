#!/usr/bin/python

import Adafruit_DHT
import time

sensor = Adafruit_DHT.DHT22
pin = 4

def read_temperature_and_humidity():

	humidity, temperature= Adafruit_DHT.read_retry(sensor,pin)
	
	if humidity is None:
		humidity = 0
	if temperature is None:
		temperature = 0

	return(temperature,humidity)